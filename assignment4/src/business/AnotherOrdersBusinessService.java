package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.*;
import javax.enterprise.inject.Alternative;

import beans.Order;

@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class AnotherOrdersBusinessService implements OrdersBusinessInterface {

	List<Order> orderList = new ArrayList<Order>();
	
	@Override
	public void test() {
		System.out.println("=========== Hello from Another Order Business service!");

	}

	
	public AnotherOrdersBusinessService() {
		orderList.add(new Order ("1100001","Another Item 1",(float)1.00,10));
		orderList.add(new Order ("1100002","Another Item 2",(float)2.00,20));
		orderList.add(new Order ("1100003","Another Item 3",(float)3.00,30));
		orderList.add(new Order ("1100004","Another Item 4",(float)4.00,40));
		orderList.add(new Order ("1100005","Another Item 5",(float)5.00,50));
		orderList.add(new Order ("1100006","Another Item 6",(float)6.00,60));
		orderList.add(new Order ("1100007","Another Item 7",(float)7.00,70));
		orderList.add(new Order ("1100008","Another Item 8",(float)8.00,80));
		orderList.add(new Order ("1100009","Another Item 9",(float)9.00,90));
		orderList.add(new Order ("1100010","Another Item 10",(float)10.00,100));
		orderList.add(new Order ("1100011","Another Item 11",(float)11.00,110));
		orderList.add(new Order ("1100012","Another Item 12",(float)12.00,120));
		orderList.add(new Order ("1100013","Another Item 13",(float)13.00,130));
		orderList.add(new Order ("1100014","Another Item 14",(float)14.00,140));
		orderList.add(new Order ("1100015","Another Item 15",(float)15.00,150));
	}
	
	@Override
	public List<Order> getOrders() {
		return orderList;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orderList = orders;
	}

}
