package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.*;
import javax.enterprise.inject.Alternative;

import beans.Order;

@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {
	List<Order> orderList = new ArrayList<Order>();
	
	@Override
	public void test() {
		System.out.println("=========== Hello from Order Business service!");

	}

	
	public OrdersBusinessService() {
		orderList.add(new Order ("0000001","Item 1",(float)1.00,1));
		orderList.add(new Order ("0000002","Item 2",(float)2.00,2));
		orderList.add(new Order ("0000003","Item 3",(float)3.00,3));
		orderList.add(new Order ("0000004","Item 4",(float)4.00,4));
		orderList.add(new Order ("0000005","Item 5",(float)5.00,5));
		orderList.add(new Order ("0000006","Item 6",(float)6.00,6));
		orderList.add(new Order ("0000007","Item 7",(float)7.00,7));
		orderList.add(new Order ("0000008","Item 8",(float)8.00,8));
		orderList.add(new Order ("0000009","Item 9",(float)9.00,9));
		orderList.add(new Order ("0000010","Item 10",(float)10.00,10));
		orderList.add(new Order ("0000011","Item 11",(float)11.00,11));
		orderList.add(new Order ("0000012","Item 12",(float)12.00,12));
		orderList.add(new Order ("0000013","Item 13",(float)13.00,13));
		orderList.add(new Order ("0000014","Item 14",(float)14.00,14));
		orderList.add(new Order ("0000015","Item 15",(float)15.00,15));
	}
	
	@Override
	public List<Order> getOrders() {
		return orderList;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orderList = orders;
	}

}
