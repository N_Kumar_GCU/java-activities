package contollers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.User;
import business.MyTimerService;
import business.OrdersBusinessInterface;

@ManagedBean
public class FromContoller {

	@Inject
	OrdersBusinessInterface services;
	
	@EJB
	MyTimerService timer;
	
	public String onSubmit() {
		//get the user values from the inputs
		FacesContext context = FacesContext.getCurrentInstance();
		User newUser = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		
		//print out the test msg from order business class
		services.test();
		
		//put the user object into the POST request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", newUser);
		
		//start timer
		timer.setTimer(10000);
		
		//show the next page
		return "Responce.xhtml";
	}
	
	public OrdersBusinessInterface getService() {
		return services;
	}
}
