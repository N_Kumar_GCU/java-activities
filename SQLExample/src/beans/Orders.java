package beans;

public class Orders {
	int orderID;
	int orderNumber;
	String product;
	double price;
	int quant;
	
	
	public Orders(int orderID, int orderNumber, String product, double price, int quant) {
		super();
		this.orderID = orderID;
		this.orderNumber = orderNumber;
		this.product = product;
		this.price = price;
		this.quant = quant;
	}
	
	
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuant() {
		return quant;
	}
	public void setQuant(int quant) {
		this.quant = quant;
	}
	
	
}
