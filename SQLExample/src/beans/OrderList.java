package beans;

import java.util.ArrayList;

public class OrderList {
	ArrayList<Orders> thelist = new ArrayList<Orders>();
	
	public void add(Orders o) {
		thelist.add(o);
	}
	
	public void printAll() {
		System.out.println("========Order List========");
		
		for(Orders o :thelist) {
			System.out.println("OrderID " + o.orderID + ": " + o.orderNumber+" -- "+o.product+"  ($"+o.price+")  QTY:"+o.quant);
		}
		
		System.out.println("========End List========");
	}
}
