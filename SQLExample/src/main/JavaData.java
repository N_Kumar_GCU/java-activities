package main;

import beans.OrderList;
import beans.Orders;

public class JavaData {

	public static void main(String[] args) {
		Orders o1 = new Orders(1, 100001, "Product 1", 1.50, 10);
		Orders o2 = new Orders(2, 100002, "Product 2", 12, 22);
		Orders o3 = new Orders(3, 100003, "Product 3", 3.50, 30);
		
		OrderList orderList = new OrderList();
		orderList.add(o1);
		orderList.add(o2);
		orderList.add(o3);
		
		orderList.printAll();
		
	}

}
