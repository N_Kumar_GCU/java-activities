package main;

import java.sql.*;

public class ReadSQLData {

	public static void main(String[] args) throws SQLException {
		//Connect to database
		String url = "jdbc:mysql://localhost: 8889/JavaWeb";
		String user ="root";
		String pass = "root";
		
		Connection c = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			c = DriverManager.getConnection(url,user,pass);
			System.out.println("Sucess! \nURL:" +url +"\nUser:"+user+"\nPassword:"+pass);
			
			//SQl statement
			stmt = c.createStatement();
			//execute SQL
			rs = stmt.executeQuery("SELECT * FROM JavaWeb.testOrders");
			//display result
			while(rs.next()) {
				System.out.println("OrderID " + rs.getInt("orderID") + ": " +rs.getInt("orderNumber")+" -- "+rs.getString("product")+"  ($"+rs.getDouble("price")+")  QTY:"+rs.getInt("quant"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error!");
			e.printStackTrace();
		} finally {
			//close connection
			rs.close();
			stmt.close();
			c.close();
			
		}

	}

}
