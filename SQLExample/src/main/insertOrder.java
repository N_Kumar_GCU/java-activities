package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class insertOrder {

	public static void main(String[] args) throws SQLException {
		//Connect to database
				String url = "jdbc:mysql://localhost: 8889/JavaWeb";
				String user ="root";
				String pass = "root";
				
				Connection c = null;
				Statement stmt = null;
				int rowsAffected = 0;
				
				try {
					c = DriverManager.getConnection(url,user,pass);
					System.out.println("Sucess! \nURL:" +url +"\nUser:"+user+"\nPassword:"+pass);
					
					//SQl statement
					stmt = c.createStatement();
					//execute SQL
					rowsAffected = stmt.executeUpdate("insert into JavaWeb.testOrders(orderID, orderNumber, product, price, quant) values (null, '1111010', 'New Item 11', '11.99', '123') ");
					//Success message
					System.out.println("Rows Affected: "+ rowsAffected);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					System.out.println("Error!");
					e.printStackTrace();
				} finally {
					//close connection
					stmt.close();
					c.close();
					
				}

			}
	}
