package business;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.enterprise.inject.Alternative;
import javax.jms.*;

import beans.Order;

@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {
	List<Order> orderList = new ArrayList<Order>();
	
	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;

	
	
	@Override
	public void test() {
		System.out.println("=========== Hello from Order Business service!");

	}

	
	public OrdersBusinessService() {
		orderList.add(new Order ("0000001","Item 1",(float)1.25,1));
		orderList.add(new Order ("0000002","Item 2",(float)2.50,2));
		orderList.add(new Order ("0000003","Item 3",(float)3.75,3));
		orderList.add(new Order ("0000004","Item 4",(float)4.10,4));
		orderList.add(new Order ("0000005","Item 5",(float)5.99,5));
		orderList.add(new Order ("0000006","Item 6",(float)6.80,6));
		orderList.add(new Order ("0000007","Item 7",(float)7.50,7));
		orderList.add(new Order ("0000008","Item 8",(float)8.25,8));
		orderList.add(new Order ("0000009","Item 9",(float)9.99,9));
		orderList.add(new Order ("0000010","Item 10",(float)10.10,10));
		orderList.add(new Order ("0000011","Item 11",(float)11.99,11));
		orderList.add(new Order ("0000012","Item 12",(float)12.99,12));
		orderList.add(new Order ("0000013","Item 13",(float)13.60,13));
		orderList.add(new Order ("0000014","Item 14",(float)14.95,14));
		orderList.add(new Order ("0000015","Item 15",(float)15.50,15));
	}
	
	@Override
	public List<Order> getOrders() {
		return orderList;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orderList = orders;
	}


	@Override
	public void sendOrder(Order order) {
		// Send a Message for an Order
				try 
				{
					Connection connection = connectionFactory.createConnection();
					Session  session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
					MessageProducer messageProducer = session.createProducer(queue);
					TextMessage message1 = session.createTextMessage();
					message1.setText("This is test message");
					messageProducer.send(message1);
					connection.close();
				} 
				catch (JMSException e) 
				{
					e.printStackTrace();
				}
	}

}
