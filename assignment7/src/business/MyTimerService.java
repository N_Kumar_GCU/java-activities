package business;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

@Stateless
public class MyTimerService {
	@Resource
	TimerService timerService;
	
	//private static final Logger logger = Logger.getLogger("business.MyTimerService");
	
	public MyTimerService() {
		
	}
	
	public void setTimer(long interval) {
		timerService.createTimer(interval, "Set up my timer...");
	}
	
	//@Timeout
	//public void programmicTimer(Timer timer) {
		//logger.info("@Timeout/Programmic Timer ---  " + new java.util.Date());
	//}
	
	//@Schedule(second="*/10",minute="*",hour="0-23")
	//private void scheduledTimer(final Timer t) {
		//logger.info("@Scheduled Timer ---  " + new java.util.Date());
	//}
}
