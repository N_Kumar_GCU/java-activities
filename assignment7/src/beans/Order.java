package beans;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlRootElement;

@ManagedBean
@XmlRootElement(name="Order")
public class Order {
	String orderNum = "";
	String productName ="";
	float price = 0;
	int quant = 0;
	
	public Order() {
		super();
	}
	public Order(String orderNum, String productName, float price, int quant) {
		super();
		this.orderNum = orderNum;
		this.productName = productName;
		this.price = price;
		this.quant = quant;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuant() {
		return quant;
	}
	public void setQuant(int quant) {
		this.quant = quant;
	}
	
	
}
