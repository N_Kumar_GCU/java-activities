package beans;

import java.util.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
@ManagedBean
@ViewScoped
public class Orders {
	List <Order> orderList = new ArrayList<Order>();
	
	public Orders() {
		orderList.add(new Order ("0000001","Item 1",(float)1.00,11));
		orderList.add(new Order ("0000002","Item 2",(float)2.00,22));
		orderList.add(new Order ("0000003","Item 3",(float)3.00,33));
		orderList.add(new Order ("0000004","Item 4",(float)4.00,44));
		orderList.add(new Order ("0000005","Item 5",(float)5.00,55));
		orderList.add(new Order ("0000006","Item 6",(float)6.00,66));
		orderList.add(new Order ("0000007","Item 7",(float)7.00,77));
		orderList.add(new Order ("0000008","Item 8",(float)8.00,88));
		orderList.add(new Order ("0000009","Item 9",(float)9.00,99));
		orderList.add(new Order ("0000010","Item 10",(float)10.00,1010));
		orderList.add(new Order ("0000011","Item 11",(float)11.00,1111));
		orderList.add(new Order ("0000012","Item 12",(float)12.00,1212));
		orderList.add(new Order ("0000013","Item 13",(float)13.00,1313));
		orderList.add(new Order ("0000014","Item 14",(float)14.00,1414));
		orderList.add(new Order ("0000015","Item 15",(float)15.00,1515));
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	
}
