<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Respond</title>
</head>
<body>
<h2>Hello <%= request.getAttribute("firstname") %>,</h2>
<h4>Your last name is <%= request.getAttribute("lastname") %>.</h4>
<br>
Nice to meet you <%=request.getAttribute("firstname") %> <%= request.getAttribute("lastname") %>.
</body>
</html>