package assignmentPack;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//collect info from the form
		String firstNameEntered = request.getParameter("firstname");
		String lastNameEntered = request.getParameter("lastname");
		
		//make variables readable by this request, so that it can be passed to another page
		request.setAttribute("firstname", firstNameEntered);
		request.setAttribute("lastname", lastNameEntered);
		
		if (firstNameEntered == "") {
			//go to another page
			request.getRequestDispatcher("TestError.jsp").forward(request, response);
		}else if (lastNameEntered == "") {
			//go to another page
			request.getRequestDispatcher("TestError.jsp").forward(request, response);
		}else{
			//go to another page
			request.getRequestDispatcher("TestRespond.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
