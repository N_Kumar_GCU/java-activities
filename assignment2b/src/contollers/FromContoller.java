package contollers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import beans.User;

@ManagedBean
public class FromContoller {

	public String onSubmit() {
		//get the user values from the inputs
		FacesContext context = FacesContext.getCurrentInstance();
		User newUser = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		
		//put the user object into the POST request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", newUser);
		
		//show the next page
		return "Responce.xhtml";
	}
	
	public String onFlash() {
		//get the user values from the inputs
		FacesContext context = FacesContext.getCurrentInstance();
		User newUser = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		
		//put the user object into the POST request
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("user", newUser);
		
		//show the next page
		return "Responce2.xhtml";
	}
}
